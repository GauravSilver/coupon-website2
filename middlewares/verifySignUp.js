const mongoose = require('mongoose');
const users = require('../models/users');
const async = require('async_hooks');
const { login } = require('../controllers/users/usersCntrl');

checkDuplicateUsernameOrEmail = (req, res, next) => {
  // Username
  users.findOne({
    where: {
      username: req.body.username,
      email: req.body.email
    }
  }).then(user => {
    if (user) {
      res.status(400).send({
        message: "Failed! Username is already in use!"
      });
      return;
    }
    // Email
    users.findOne({
      where: {
        username: req.body.username,
        email: req.body.email
      }
    }).then(user => {
      if (user) {
        res.status(400).send({
          message: "Failed! Email is already in use!"
        });
        return;
      }
      next();
    });
  });
};




const verifySignUp = {
  checkDuplicateUsernameOrEmail: checkDuplicateUsernameOrEmail
}
module.exports = {login};