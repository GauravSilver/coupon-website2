'use strict'
const jwt = require("jsonwebtoken");
const mongoose = require('mongoose');
const isset = require("isset");
const users = mongoose.users;
const verifyToken = (req, res, next) => {
  let token = req.headers["SECRET_KEY"];
  if (!token) {
    return res.status(403).send({
      message: "No token provided!"
    });
  }
  jwt.verify(token, config.secret, (err, decoded) => {
    if (err) {
      return res.status(401).send({
        message: "Unauthorized!"
      });
    }
    req.userId = decoded.id;
    next();
  });
};


module.exports = {verifyToken};
