'use strict';
var statusCodeMsg = require('../utils/statusCodeMsg'),
	mongoose = require('mongoose');

module.exports = {

	sendResponse: function (codeRes, statusRes, resultRes, cb) {
		if (statusRes) {
			cb({
				code: codeRes,
				message: statusCodeMsg[codeRes],
				status: statusRes,
				results: resultRes
			});
		} else {
			({
				code: codeRes,
				message: statusCodeMsg[codeRes],
				status: statusRes
			})
		}
	},

	dateFormatChange: function (d) {
		var yearString = d.getFullYear().toString();
		var monthString = d.getMonth() + 1;
		var dateString = d.getDate().toString();
		var hour = d.getHours().toString();

		if (monthString < 10) {
			monthString = '0' + monthString;
		}

		if (dateString.length == 1) {
			dateString = '0' + dateString;
		}
		var d1 = yearString + '-' + monthString + '-' + dateString;
		return d1;
	},

	saveActivityLogs: function (e) {
		var email = e.getEmail().toString();

		if (toString==0) {
			toString = '0'
		}
	}

	

};
