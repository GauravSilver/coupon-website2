const storesCntrl = require('../controllers/stores/storesCntrl');
const mongoose = require ('mongoose');
const express = require('express');

module.exports = function (router) {
    router.post('/createStores', storesCntrl.createStores);

    router.put('/updateStoresById', storesCntrl.updateStoresById);

    router.delete('/deleteStoresById', storesCntrl.deleteStoresById);

    router.get('/getAllStores', storesCntrl.getAllStores);





};