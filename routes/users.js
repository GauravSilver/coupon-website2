const express = require('express');
const router = express.Router();
const verifySignUp = require('../middlewares/verifySignUp');
const usersCntrl = require('../controllers/users/usersCntrl');
const authenticateJWT= require('../utils/jwt-authentication');

module.exports = function (router){


    router.post('/signUp',  usersCntrl.signUp);

    // router.post('/user/login',   usersCntrl.login);

    // router.post('/login',  usersCntrl.login );

    // router.post('/createUser',  usersCntrl.createUser );

    router.put('/updateUserById',  usersCntrl.updateUserById );

    router.delete('/deleteUserById',  usersCntrl.deleteUserById );

    router.get('/getAllUser',  usersCntrl.getAllUser );

    router.post('/getUserByEmail',  usersCntrl.getUserByEmail );


};
