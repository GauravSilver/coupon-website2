const authenticateJWT= require('../utils/jwt-authentication');
const couponsCntrl = require('../controllers/coupons/couponsCntrl');
const mongoose = require ('mongoose');
const express = require('express');

module.exports = function (router) {

    router.get('/getAllCoupons', couponsCntrl.getAllCoupons);

    router.put('/updateCouponsById', couponsCntrl.updateCouponsById);

    router.post('/createCoupons', couponsCntrl.createCoupons);
    
    router.delete('/deleteCoupons' , couponsCntrl.deleteCouponsById);




};