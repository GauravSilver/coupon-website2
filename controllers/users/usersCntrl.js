const userObj = require('./../../models/users');
const User = require('./../../models/users');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');
const mongoose = require('mongoose');
const secret = 'project2022'
const jwt = require('jsonwebtoken');
const generateToken = require('jsonwebtoken');
const commonFunction = require('../../utils/commonFunction');
const isSet = require('isset');
const authJwt = require('../../middlewares/authJwt');
const { authenticateJWT } = require('../../utils/jwt-authentication');
const empty = require('is-empty');
const { response } = require('express');





// Save user to Database
// const signUp = async function (req, res) {
//     try {
//         console.log("testingsss")
//         const { body } = req;
//         if (body.email && body.email && body.password) {
//             let userObj = {
//                 email: body.email,
//                 username: body.username,
//                 password: body.password,
//                 phone: body.phone,
//                 status: body.status
//             } = req.body

//             var userExists = await getUserByEmail(body.email); 
//             console.log("userexisttttttttttt", userExists)
            
//             if (!userExists) {
//                 console.log("if condition")
//                     if (err) {
//                         console.log("error in create user");
//                         commonFunction.sendResponse(400, false, null, function (response) {
//                             res.json(response);
//                         });
//                         console.log('userObj', userObj)
//                     } else {
//                         let updateMsg = req.body.user + " has registered a user";
//                         commonFunction.sendResponse(201, true, resp, function (response) {
//                             res.json(response);
//                         });
//                     }
//                 ;
                
//             }
//         } else {
//             console.log('All fields Required!');
//             commonFunction.sendResponse(409, false, null, function (response) {
//                 res.json(response);commonFunction.sendResponse(500, false, null, function (response) {
//                     res.json(response);
//                 });
//             });
//         }
//         // var userObj = req.body.email ? req.body : req.body.data;
//         // var userObj = new User({
//         //     email: userObj.email,
//         //     name: userObj.name,
//         //     country: userObj.country,
//         //     company: userObj.company,
//         //     phone: userObj.phone,
//         //     password: userObj.password,
//         //     access: 1
//         // });
//         // try {
//         //     var userExists = await getUserByEmail(userObj.email);
//         //     if (userExists.length == 0) {
//         //         userObj.save(function (err, resp) {
//         //             if (err) {
//         //                 console.log("error in create user");
//         //                 commonFunction.sendResponse(400, false, null, function (response) {
//         //                     res.json(response);
//         //                 });
//         //                 console.log('userObj', userObj)


//         //             } else {
//         //                 let updateMsg = req.body.user + " has registered a user";
//         //                 commonFunction.sendResponse(201, true, resp, function (response) {
//         //                     res.json(response);
//         //                 });
//         //             }
//         //         });
//         //     }
//         // } catch (e) {
//         //     commonFunction.sendResponse(400, false, null, function (response) {
//         //         res.json(response);
//         //     });
//         // }
//     } catch (err) {
//         console.log(`Error in registerUser`, err);
//         commonFunction.sendResponse(500, false, null, function (response) {
//             res.json(response);
//         });
//     }
// }



const signUp = async function (req, res) {
    let result = {
        success: true,
        message: '',
        data: {}
    };
    try {
        const { body } = req;
        if (body.email && body.email && body.password) {
            let userObj = {
                email: body.email,
                username: body.username,
                password: body.password,
                phone: body.phone,
                status: body.status
            } = req.body
        const existingUser = await getUserByEmail(body.email);
        // console.log("existingUser",existingUser)
        if (existingUser) {
            result.success = false;
            result.message = "User already exists";
            return result;
        }
        const userDetails = await save(body.email,body.password)
        console.log("userDetails", userDetails)
        result.message = "User Signed up successfully"
        return result 
   } } catch (err) {
        console.log("Error while signup in :: ", err);
        result.success = false;
        result.message = "Error while signup";
        return result;
    }
    commonFunction.sendResponse(500, false, null, function (response){
        res.json(response);
    });
}

function getUserByEmail(emailUser) {
    return new Promise(function(resolve, reject) {
        User.findOne({
                email: emailUser
            },
            function(err, res) {
                if (err || !res) {
                    console.log("Err in getUserByEmail", err);
                    resolve(false)
                } else {
                    resolve(true);
                }
            })
    })
}

function save(emailUser , password) {
    console.log("testing",emailUser , password)
    let testing1 = new User({   email: emailUser,
        password: password})
    return new Promise(function(resolve, reject) {
        testing1.save({
                email: emailUser,
                password: password
            },
            function(err, res) {
                if (err || !res) {
                    console.log("Err in getUserByEmail", err);
                    resolve(false)
                } else {
                    resolve(true);
                }
            })
    })
}






// User Login



// // Creates new user
// const createUser = function (req, res){
//     console.log('*****New User Created*****');
//     User.create({
//         username: req.body.username,
//         email: req.body.email,
//         password: req.body.password,
//         phone: req.body.phone,
//         status: req.body.status,
//     })

//     .then (User => {
//         console.log("user data : ", User);
//         res.send({ message: "User was created successfully"})
//     })
//     .catch(err => {
//         res.status(500).send({ message: err.message})
//     });
// };


// updates User
const updateUserById = function (req, res) {
    try {
        console.log("*****update User By Id*****");

        const {
            username,
            email,
            password,
            phone,
            status
        } = req.body;

        User.findOneAndUpdate({
            username: username,
            email: email,
            password: password,
            phone: phone,
            status: status
        }).exec(function (err, result) {
            if (err) {
                console.log(err);
                commonFunction.sendResponse(400, false, null, function (response) {
                    res.json(response);
                });
            } else {
                commonFunction.sendResponse(202, true, result, function (response) {
                    res.json(response);
                });
            }
        });
    } catch (err) {
        console.log('Error: >>>', err);
        commonFunction.sendResponse(500, false, null, function (response) {
            res.json(response);
        });
    }
}


// Deletes User
const deleteUserById = function (req, res) {
    try {
        console.log('******delete User By Id*****');

        User.findOneAndDelete({}).exec(function (err, result) {
            console.log("err and result :::", err, result);

            if (err) {
                console.log(err);
                commonFunction.sendResponse(400, false, null, function (response) {
                    res.json(response);
                });
            } else {
                commonFunction.sendResponse(200, true, result, function (response) {
                    res.json(response);
                });
            }
        });
    } catch (err) {
        console.log('Error: ', err);
        commonFunction.sendResponse(500, false, null, function (response) {
            res.json(response);
        });
    }
}

// Get user list
const getAllUser = function (req, res) {
    try {
        console.log('*****get All User*****');
        let findQuery = {};
        User.find({ findQuery }).lean().exec(function (err, User_list) {
            if (err) {
                commonFunction.sendResponse(400, false, null, function (response) {
                    res.json(response);
                });
            } else {
                console.log('user_list: ', User_list.length);
                if (User_list.length > 0) {
                    commonFunction.sendResponse(200, true, User_list, function (response) {
                        res.json(response);
                    });
                }
            }
        })
    } catch (err) {
        console.log('Error: >>>', err);
        commonFunction.sendResponse(500, false, null, function (response) {
            res.json(response);
        });
    }
}


// const getUserByEmail = function (req, res, email) {
//     console.log("email", email)
//     try {
//          let testing = email
//         // console.log('offerid', offer_id)
//         User.find({ testing }, (err, log) => {
//             console.log("data--->", err, log);
//             if (err) {
//                 commonFunction.sendResponse(400, false, err, function (response) {
//                     res.json(response);
//                 });
//             } else {
//                 commonFunction.sendResponse(201, true, log, function (response) {
//                     res.json(response);
//                 });
//             }
//         });
//     } catch (err) {
//     }
// }

module.exports = {
    signUp,
    //   login,
    //   createUser,
    updateUserById,
    deleteUserById,
    getAllUser,
    getUserByEmail
}