const commonFunction = require('./../../utils/commonFunction');
const stores = require('./../../models/stores');
const  response  = require('express');

const createStores = function (req, res) {
    console.log("req.body")

    try {
        console.log('*****create Store*****');

        const {
            name,
            site_url,
            description,
            currency,
            rating,
            categories,
            status,
            id,
            activation_date,
            modified_date,
            show_products_links,
        } = req.body;
        var StoreAdd = stores ({

            name: name,
            site_url: site_url,
            description: description,
            currency: currency,
            rating: rating,
            categories: categories,
            status: status,
            id: id,
            activation_date: activation_date,
            modified_date:modified_date,
            show_products_links: show_products_links

        });
        StoreAdd.save((err, stores) => {
            if (err) {
                commonFunction.sendResponse(500, false, null, function (response){
                    res.json(response);
                });
                
            } else {
                commonFunction.sendResponse(201, true, stores, function(response){
                    res.json(response);
                });
            }
        });
    } catch (err) {
        console.log('Error: >>>', err);
        commonFunction.sendResponse(500, false, null, function (response){
            res.json(response);
        })
    }
}

const updateStoresById = function (req, res) {
    try {
        console.log('*****update Store By Id*****');


        const {
            name,
            site_url,
            description,
            currency,
            rating,
            categories,
            status,
            id,
            activation_date,
            modified_date,
            show_products_links,
        } = req.body;

        stores.findOneAndUpdate( {
            name: name,
            site_url: site_url,
            description: description,
            currency: currency,
            rating: rating,
            categories: categories,
            status: status,
            id: id,
            activation_date: activation_date,
            modified_date:modified_date,
            show_products_links: show_products_links
        }).exec(function(err, result){
            if (err) {
                console.log(err);
                commonFunction.sendResponse(400, false, null, function(response){
                    res.json(response);
                });
            } else {
                commonFunction.sendResponse(202, true, result, function(response){
                    res.json(response);
                });
            }
        });
        } catch (err) {
            console.log('Error: >>>', err);
            commonFunction.sendResponse(500, false, null, function(response){
                res.json(response);
            });
        }
        
}

const deleteStoresById = function (req, res) {
    try { 
        console.log('*****delete Stores By Id*****');

        stores.findOneAndDelete({}).exec(function(err, result){
            console.log("err and result :::", err, result);

            if(err) {
                console.log(err);
                commonFunction.sendResponse(400, false, null, function(response){
                    res.json(response);
                });
            } else {
                commonFunction.sendResponse(200, true, result, function(response){
                    res.json(response);
                });
            }
        });
        } catch (err){
            console.log('Error: ', err);
            commonFunction.sendResponse(500, false, null, function (response){
                res.json(response);
            });
        }
}

const getAllStores = function (req, res){
    try {
        console.log('*****get All Stores*****');
        let findQuery = {};
        console.log('***888***');

        stores.find({findQuery}).lean().exec(function (err, stores_list){
            if (err) {
                commonFunction.sendResponse(400, false, null, function (response){
                    res.json(response);
                });
            } else {
                console.log('stores_list: ', stores_list.length);
                if (stores_list.length > 0) {
                    commonFunction.sendResponse(200, true, stores_list, function(response) {
                        res.json(response);
                    });
                }
            }
        })
    } catch (err) {
        console.log('Error: >>>', err);
        commonFunction.sendResponse(500, false, null, function (response){
            res.json(response);
        });
    }
}



module.exports = {
    createStores,
    updateStoresById,
    deleteStoresById,
    getAllStores


}