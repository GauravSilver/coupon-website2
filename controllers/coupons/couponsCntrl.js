const commonFunction = require('../../utils/commonFunction');
const Coupons = require('./../../models/coupons');

const  getAllCoupons = function (req, res) {
    try {
        console.log('***** get coupons list *****');
        let findQuery = {};
        Coupons.find({findQuery}).lean().exec(function (err, Coupons_list) {
            if (err) {
                commonFunction.sendResponse(400, false, null, function (response) {
                    res.json(response);
                });
            } else {
                console.log('coupons_list : ', Coupons_list.length);
                if (Coupons_list.length > 0) {
                    commonFunction.sendResponse(200, true, Coupons_list, function (response) {
                        res.json(response);
                    });
                }
            }
        })
    } catch (err) {
        console.log(`Error:  >>> `, err);
        commonFunction.sendResponse(500, false, null, function (response) {
            res.json(response);
        });
    }
}



const createCoupons = function (req, res) {
    console.log("req.body")


        console.log('***** create coupons *****');

        Coupons.create({
            status: req.body.status,
            rating: req.body.rating,
            campaign: req.body.campaign,
            description: req.body.description,                                                  
            short_name: req.body.short_name,
            exclusive: req.body.exclusive,
            date_end: req.body.date_end,
            date_start: req.body.date_start,
            id: req.body.id,
            regions: req.body.regions,
            discount: req.body.discount,
            types: req.body.types,
            image: req.body.image,
            species: req.body.species,
            categories: req.body.categories,
            name: req.body.name,
            language: req.body.language,
            is_unique: req.body.is_unique,
            is_personal: req.body.is_personal
        })
    
    .then (Coupons => {
        console.log("user data : ", Coupons);
        res.send({ message: "Coupons was created successfully"});
    })
    .catch(err => {
        res.status(500).send ({ message: err.message})
    });
};




const updateCouponsById = function (req, res) {
    try {
        console.log('***** update coupons By Id *****');
       
        const {
           status,
           rating,
           campaign,
           description,
           short_name,
           exclusive,
           date_end,
           date_start,
           id,
           regions,
           discount,
           types,
           image,
           species,
           categories,
           name,
           language,
           is_unique,
           is_personal

        } = req.body;
        Coupons.findOneAndUpdate( {
            status: status,
            rating: rating,
            campaign: campaign,
            description: description,                                                  
            short_name: short_name,
            exclusive: exclusive,
            date_end: date_end,
            date_start: date_start,
            id: id,
            regions: regions,
            discount: discount,
            types: types,
            image: image,
            species: species,
            categories: categories,
            name: name,
            language: language,
            is_unique: is_unique,
            is_personal: is_personal
        }).exec(function(err, result) {
            if (err) {
                console.log(err);
                commonFunction.sendResponse(400, false, null, function(response) {
                    res.json(response);
                });
            } else {
                commonFunction.sendResponse(202, true, result, function(response) {
                    res.json(response);
                });
            }
        });
    } catch (err) {
        console.log(`Error:  >>> `, err);
        commonFunction.sendResponse(500, false, null, function (response) {
            res.json(response);
        });
    }
}


const deleteCouponsById = function (req, res) {
    try {
        console.log('***** delete coupons By Id *****');
        var  coupons_id  = req.params._id;
        let findQuery = {
            _id: coupons_id
        }

        Coupons.findOneAndDelete({findQuery}).exec(function(err, result) {
            console.log("err and result :::: ", err, result);         

            if (err) {
                console.log(err);
                commonFunction.sendResponse(400, false, null, function(response) {
                    res.json(response);
                });
                
            } else {
                commonFunction.sendResponse(200, true, result, function(response) {
                    res.json(response);
                });
            }
        });
    } catch (err) {
        console.log(`Error:  >>> `, err);
        commonFunction.sendResponse(500, false, null, function (response) {
            res.json(response);
        });
    }
}




module.exports = {
    getAllCoupons,
    updateCouponsById,
    createCoupons,
    deleteCouponsById


};