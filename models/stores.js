'use strict'
const mongoose = require('mongoose'),
Schema = mongoose.Schema;
var storeSchema = new Schema({
    name: String,
    site_url: String,
    description: String,
    currency: String,
    rating: String,
    categories: Array,
    status: String,
    id: Number,
    image: String,
    activation_date: String,
    modified_date: Array,
    show_products_links: Boolean,
    createdAt: {
		type: Date,
		default: Date.now()
	},
    updatedAt: {
		type: Date,
		default: Date.now()
	}
   }, {collection :'stores'});
    
module.exports = mongoose.model('stores', storeSchema);