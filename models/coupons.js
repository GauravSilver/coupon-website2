'use strict'
const mongoose = require('mongoose'),
Schema = mongoose.Schema;
var couponsSchema = Schema({
    status: Boolean,
    rating: String,
    campaign: String,
    description: String,
    short_name: String,
    exclusive: Boolean,
    date_end: String,
    date_start: String,
    id: Number,
    regions: Array,
    discount: String,
    types: Array,
    image: String,
    species: String,
    categories: Array,
    name: String,
    language: String,
    is_unique: Boolean,
    is_personal: Boolean,
    createdAt: {
		type: Date,
		default: Date.now()
	},
    updatedAt: {
		type: Date,
		default: Date.now()
	}
}, {collection : 'coupons'});


module.exports = mongoose.model('coupons', couponsSchema);
