'use-strict'
const mongoose = require('mongoose');
mongoose.Schema = mongoose.Schema;
const uniqueValidator = require('mongoose-unique-validator');
var userSchema = new mongoose.Schema({
    // _id: mongoose.Schema.Types.ObjectId,
    username: String,
    email: String,
    password: String, 
    phone: Number,
    status: Boolean,
    createdAt: {
                 type: Date,
                 default: Date.now()
             },
     updatedAt: {
                 type: Date,
                 default: Date.now()
             },

}, {collection: 'user'});

module.exports = mongoose.model('user', userSchema);